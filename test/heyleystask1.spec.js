require('mocha');

const { assert } = require('chai'); // Using Assert style
const { requireBox } = require('@liquidapps/box-utils');

const artifacts = requireBox('seed-eos/tools/eos/artifacts');
const deployer = requireBox('seed-eos/tools/eos/deployer');
const { genAllocateDAPPTokens } = requireBox('dapp-services/tools/eos/dapp-services');
const { getTestContract, getLocalDSPEos } = requireBox('seed-eos/tools/eos/utils');
const { getCreateKeys } = requireBox('eos-keystore/helpers/key-utils');

const { eosio } = requireBox('test-extensions/lib/index');

var contractCode1 = 'heyleystask1';
var contractCode2 = 'heyleysstud1';
var ctrt1 = artifacts.require(`./${contractCode1}/`);
var ctrt2 = artifacts.require(`./${contractCode2}/`);

describe(`${contractCode1} Contract`, () => {
    let testcontract1;
    let testcontract2;
    before(done => {
        (async () => {
            try {
                var deployedContract1 = await deployer.deploy(ctrt1, contractCode1);
                var deployedContract2 = await deployer.deploy(ctrt2, contractCode2);
                await deployer.setActionPermissions(deployedContract1.account, deployedContract2.account, deployedContract1.keys, "upsertmult");
                await genAllocateDAPPTokens(deployedContract1, 'cron');
                await genAllocateDAPPTokens(deployedContract1, "oracle", "pprovider1", "default");
                await genAllocateDAPPTokens(deployedContract1, "oracle", "pprovider2", "foobar");
                dspeos = await getLocalDSPEos(contractCode2);
                testcontract1 = await getTestContract(contractCode1);
                testcontract2 = await getTestContract(contractCode2);

                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('Cron check if ran and results are persisted', done => {
        (async () => {
            try {
                var res = await testcontract1.schedule({
                    seconds: 60
                }, {
                    authorization: `${contractCode1}@active`,
                    broadcast: true,
                    sign: true
                });

                await eosio.delay(100000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': contractCode2,
                    'code': contractCode2,
                    'table': 'studies',
                    'limit': 10
                });
                assert.ok(res.rows.length > 0, 'counter did not increase');
                await testcontract1.removetimer({
                    account: contractCode1
                }, {
                    authorization: `${contractCode1}@active`,
                    broadcast: true,
                    sign: true
                });
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });
});