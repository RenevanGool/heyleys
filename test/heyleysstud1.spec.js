require('mocha');

const { assert } = require('chai'); // Using Assert style
const { requireBox } = require('@liquidapps/box-utils');

const artifacts = requireBox('seed-eos/tools/eos/artifacts');
const deployer = requireBox('seed-eos/tools/eos/deployer');
const { genAllocateDAPPTokens } = requireBox('dapp-services/tools/eos/dapp-services');
const { getTestContract, getLocalDSPEos } = requireBox('seed-eos/tools/eos/utils');
const { getCreateKeys } = requireBox('eos-keystore/helpers/key-utils');

const { eosio } = requireBox('test-extensions/lib/index');

var contractCode = 'heyleysstud1';
var ctrt = artifacts.require(`./${contractCode}/`);

describe(`${contractCode} Contract`, () => {
    let testcontract;
    const code = 'heyleysstud1';
    before(done => {
        (async () => {
            try {
                var deployedContract = await deployer.deploy(ctrt, code);
                dspeos = await getLocalDSPEos(code);
                testcontract = await getTestContract(code);

                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });

    it('upsertmult', done => {
        (async () => {
            try {
                testcontract = await getTestContract(code);
                var res = await testcontract.upsertmult({
                    user: "studydata",
                    studies: [
                        {
                            id: 123,
                            nctid: "nct123",
                            orgstudyid: "test",
                            orgfullname: "testtest23",
                            verifieddate: "test23423",
                            startdate: "tasdfe",
                            completiondate: "asdfw",
                            briefsummary: "asdfasdsafasdfasdfasdf",
                            condition: "asdfasfweafsdfasdf",
                            brieftitle: "asdfasdfasdfasdf"
                        },
                        {
                            id: 123234,
                            nctid: "nct123234",
                            orgstudyid: "test",
                            orgfullname: "testtest",
                            verifieddate: "test23423",
                            startdate: "tasdfe",
                            completiondate: "asdfw",
                            briefsummary: "asdfasdsafasdfasdfasdf",
                            condition: "asdfasfweafsdfasdf",
                            brieftitle: "asdfasdfasdfasdf"
                        }
                    ]
                }, {
                    authorization: `${code}@active`,
                    broadcast: true,
                    sign: true
                });
                await eosio.delay(1000);
                res = await dspeos.getTableRows({
                    'json': true,
                    'scope': code,
                    'code': code,
                    'table': 'studies',
                    'limit': 2
                });
                assert.ok(res.rows[0].study.id == 123, 'Id of first entry is not the same as given id.');
                assert.ok(res.rows[0].study.orgfullname == 'testtest23', 'Orgfullname of first entry is not the same as given orgfullname.');
                assert.ok(res.rows[1].study.id == 123234, 'Id of second entry is not the same as given id.');
                assert.ok(res.rows[1].study.orgfullname == 'testtest', 'Orgfullname of second entry is not the same as given orgfullname.');
                done();
            }
            catch (e) {
                done(e);
            }
        })();
    });
});