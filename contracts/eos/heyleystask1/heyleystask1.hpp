#pragma once
#include <eosio/eosio.hpp>
#include <eosio/system.hpp>

#include "../dappservices/cron.hpp"
#include "../dappservices/oracle.hpp"
#include "../dappservices/multi_index.hpp"
#include <unordered_map>

#define DAPPSERVICES_ACTIONS() \
  XSIGNAL_DAPPSERVICE_ACTION \
  ORACLE_DAPPSERVICE_ACTIONS \
  CRON_DAPPSERVICE_ACTIONS
#define DAPPSERVICE_ACTIONS_COMMANDS() \
  ORACLE_SVC_COMMANDS() \
  CRON_SVC_COMMANDS()

#define CONTRACT_NAME() heyleystask1

using std::string;
using namespace std;
using namespace eosio;

struct Study
{
  uint32_t id;
  string nctid;
  string orgstudyid;
  string orgfullname;
  string verifieddate;
  string startdate;
  string completiondate;
  string briefsummary;
  string condition;
  string brieftitle;
};