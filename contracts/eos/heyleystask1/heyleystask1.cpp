#include "heyleystask1.hpp"

CONTRACT_START()

string api_call(string urlstring)
{
  std::vector<char> data(urlstring.begin(), urlstring.end());
  
  auto dataset = getURI(data, [&]( auto& results ) {
    eosio::check(results.size() > 0, "no results returned");
    auto itr = results.begin();
    return itr->result;
  });

  string datastring(dataset.begin(), dataset.end());
  return datastring;
}

std::vector<string> obtain_NCTId_of_studies(std::vector<string> arrayofstudies, string fields)
{
  std::size_t nctidpos = fields.find("\"NCTId\":[");
  std::size_t nctilength = 11;
  string nctid = fields.substr(nctidpos+11, nctilength);

  arrayofstudies.push_back(nctid);

  std::size_t endpos = fields.find("}");
  fields = fields.substr(endpos+2);

  if (fields.find("\"NCTId\":[") == std::string::npos)
  {
    return arrayofstudies;
  }

  return obtain_NCTId_of_studies(arrayofstudies, fields);
}

string obtain_studyfields_from_datastring(string datastring)
{
  std::size_t pos = datastring.find("\"StudyFields\":");
  string fields = datastring.substr(pos+15);
  return fields;
}

Study parse_studyfields_into_object(string nctid, string fields)
{
  fields = fields.substr(fields.find("],")+2);
  string keys[8] = {"BriefTitle", "BriefSummary", "Condition", "OrgStudyId", "OrgFullName", "StatusVerifiedDate", "StartDate", "CompletionDate"};
  std::unordered_map<string, string> studyfields = {};

  for (uint32_t i = 0; i < 8; i++)
  {
    std::size_t keylength = keys[i].length();
    std::size_t keypos = fields.find("\""+keys[i]+"\":[");

    std::size_t valuepos = fields.find("\"", (keypos+keylength+2));
    std::size_t valuelength = fields.find("\"", (valuepos+2));

    string value = fields.substr(valuepos+1, (valuelength - valuepos) -1);
    string key = keys[i];
    studyfields[key] = value;
  }

  struct Study study;
  study.id = std::stoull(nctid.substr(3));
  study.nctid = nctid;
  study.brieftitle = studyfields["BriefTitle"];
  study.briefsummary = studyfields["BriefSummary"];
  study.condition = studyfields["Condition"];
  study.orgstudyid = studyfields["OrgStudyId"];
  study.orgfullname = studyfields["OrgFullName"];
  study.verifieddate = studyfields["StatusVerifiedDate"];
  study.startdate = studyfields["StartDate"];
  study.completiondate = studyfields["CompletionDate"];

  return study;
}

std::vector<Study> retrieve_studydetails(std::vector<string> arrayofstudies)
{
  std::vector<Study> studies;
  for(uint32_t i = 0; i < arrayofstudies.size(); i++)
  {
    string urlstring = "https://clinicaltrials.gov/api/query/study_fields?expr="+arrayofstudies[i]+"&fields=NCTId%2CBriefTitle%2CBriefSummary%2CCondition%2COrgStudyId%2COrgFullName%2CStatusVerifiedDate%2CStartDate%2CCompletionDate&min_rnk=1&fmt=json";
    string datastring = api_call(urlstring);

    string fields = obtain_studyfields_from_datastring(datastring);
    Study study = parse_studyfields_into_object(arrayofstudies[i], fields);
    studies.push_back(study);
  }

  return studies;
}

[[eosio::action]] void schedule(int32_t seconds) {
  std::vector<char> rawPayload;
  /* Schedule timer with one minute in between */
  schedule_timer(_self, rawPayload, seconds);
}

// remove timer by scope
[[eosio::action]] void removetimer(name account) {
  remove_timer(account);
}

// remove timer by scope
[[eosio::action]] void test() {
  eosio::check(true, "wow");
}

bool timer_callback(name timer, std::vector<char> payload, uint32_t seconds) {
  string urlstring = "https://clinicaltrials.gov/api/query/study_fields?expr=AREA[ResultsFirstPostDate]MISSING+AND+AREA[StudyFirstPostDate]RANGE[01%2F01%2F2015%2C+MAX]+AND+cancer&fields=NCTId&max_rnk=10&fmt=json";
  string datastring = api_call(urlstring);
  string fields = obtain_studyfields_from_datastring(datastring);

  std::string::iterator end_pos = std::remove(fields.begin(), fields.end(), ' ');
  fields.erase(end_pos, fields.end());

  std::vector<string> emptyarray;
  auto arrayofstudies = obtain_NCTId_of_studies(emptyarray, fields);
  std::vector<Study> studies = retrieve_studydetails(arrayofstudies);

  for (uint32_t i = 0; i < studies.size(); i++)
  {
    action(
      permission_level{get_self(),"active"_n},
      name("heyleysstud1"),
      name("upsert"),
      std::make_tuple(get_self(), studies[i])
    ).send();
  }

  return true;
}

CONTRACT_END((schedule)(removetimer)(test))

