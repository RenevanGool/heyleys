#pragma once
#include <eosio/eosio.hpp>
#include <eosio/system.hpp>

#include "../dappservices/cron.hpp"
#include "../dappservices/oracle.hpp"
#include "../dappservices/multi_index.hpp"

#define DAPPSERVICES_ACTIONS() \
  XSIGNAL_DAPPSERVICE_ACTION \
  IPFS_DAPPSERVICE_ACTIONS
#define DAPPSERVICE_ACTIONS_COMMANDS() \
  IPFS_SVC_COMMANDS()

#define CONTRACT_NAME() heyleysstud1

using std::string;
using namespace std;
using namespace eosio;

struct Study
{
  uint32_t id;
  string nctid;
  string orgstudyid;
  string orgfullname;
  string verifieddate;
  string startdate;
  string completiondate;
  string briefsummary;
  string condition;
  string brieftitle;
  EOSLIB_SERIALIZE(Study, (id)(nctid)(orgstudyid)(orgfullname)(verifieddate)(startdate)(completiondate)(briefsummary)(condition)(brieftitle))
};