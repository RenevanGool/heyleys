#include "heyleysstud1.hpp"

CONTRACT_START()

ACTION upsertmult(name user, std::vector<Study> studies) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  for (Study study : studies)
  {
    upsert(user, study);
  }
}

ACTION upsert(name user, Study study) {
  if (user==name("heyleystask1"))
  {
    require_auth(user);
  } else {
    require_auth(_self);
  }

  studies_def studiestable(get_self(), _self.value);
  auto iterator = studiestable.find(study.id);
  if( iterator == studiestable.end() )
  {
    studiestable.emplace(get_self(), [&]( auto& row ) {
      row.study = study;
    });
  }
  else {
    studiestable.modify(iterator, get_self(), [&]( auto& row ) {
      row.study = study;
    });
  }
}

private:
  TABLE studies {
    Study study;
    auto primary_key() const { return study.id; }
  };
  typedef eosio::multi_index<name("studies"), studies> studies_def;

CONTRACT_END((upsertmult)(upsert))

